package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutowired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.LagouSecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @LagouSecurity 控制访问权限
 * http://localhost:8080/security/query?username=zhangsan 可以访问，username=wangwu页面报错
 * http://localhost:8080/security/query2?username=zhangsan 可以访问，username=wangwu可以访问，username=chenliu页面报错
 */
@LagouController
@LagouRequestMapping("/security")
@LagouSecurity({"zhangsan", "lisi"})
public class SecurityController {

    @LagouAutowired
    private IDemoService demoService;

    @LagouRequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response,String username) {
        return demoService.get(username);
    }

    @LagouRequestMapping("/query2")
    @LagouSecurity({"wangwu"})
    public String query2(HttpServletRequest request, HttpServletResponse response,String username) {
        return demoService.get(username);
    }
}
